#!/usr/bin/env bash

STYLUS_ID=$(xsetwacom --list | grep STYLUS | sed -E 's/^.*\s+id\:\s+([[:digit:]]+)\s+.*$/\1/g')
PAD_ID=$(xsetwacom --list | grep PAD | sed -E 's/^.*\s+id\:\s+([[:digit:]]+)\s+.*$/\1/g')

PRIMARY_DISPLAY_GEOMETRY=$(xrandr | grep primary | awk '{print $4}')

# Map output of tablet to primary screen
xsetwacom --set "${STYLUS_ID}" MapToOutput ${PRIMARY_DISPLAY_GEOMETRY}

# Map stylus buttons
# On my wacom, button 1 is touch, 2 bottom, 3 top
xsetwacom --set "${STYLUS_ID}" Button 3 "key +ctrl z -ctrl"

# Map tablet buttons
xsetwacom --set "${PAD_ID}" Button 1 "key +ctrl"
xsetwacom --set "${PAD_ID}" Button 2 "key +shift"
xsetwacom --set "${PAD_ID}" Button 3 10
xsetwacom --set "${PAD_ID}" Button 8 11
