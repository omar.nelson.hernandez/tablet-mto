# tablet-mto (Formerly tablet MapToOutput but I need more stuff here now)

Automatically map your tablet to your primary display and remap the stylus and pad buttons to
something more convenient

## Installation

### Pre-requisites

#### Debian

- xserver-xorg-input-wacom (to map the tablet to the correct screen)
- xrandr (to detect the correct screen and geometry)
- grep
- sed
- awk
- xev (to check the button ID for both stylus and pad)

#### Standard

- Clone
- (Optional) put the shell script in your PATH
- (Optional) edit to your liking
- (Optional) call this script from a startup script so it maps the current device ID to the current
  primary output

## Checking button IDs

1. Do `xev -event button` and press whatever button you want to check
2. Inspect your output and on the third line (per event, one press and one release) you will see the
   button ID, like below:

```
ButtonPress event, serial 25, synthetic NO, window 0x3e00001,
    root 0x1e9, subw 0x0, time 49147936, (215,714), root:(1502,759),
    state 0x10, button 3, same_screen YES

ButtonRelease event, serial 25, synthetic NO, window 0x3e00001,
    root 0x1e9, subw 0x0, time 49148446, (225,718), root:(1512,763),
    state 0x410, button 3, same_screen YES
```

3. (Optional) if for whatever reason the program where you want to use your new keybinds doesn't
   want to use buttons to map functions (I'm looking at you Krita), remap the buttons (Button 11, 12
   ,13 ,14) to fake F keys (F21, F22, F23, F24) by using xdotools or something similar. Check
   https://gitlab.com/omar.nelson.hernandez/eitr in folder roles/xbindkeys where I map buttons 11,
   12, 13, 14 to F21, F22, F23, F24. Some programs have an issue with button IDs but are completely
   fine with fake F keys.

## Additional information

- https://wiki.archlinux.org/title/Graphics_tablet
